import { NgModule, ErrorHandler, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { RestProvider } from '../providers/rest/rest';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2Webstorage } from 'ng2-webstorage';
import { CookieService } from 'ngx-cookie';
import { LoginPage } from '../pages/login/login';
import { LoginService } from '../pages/login/login.service';
import { HttpModule } from '@angular/http';
import { JhiEventManager } from 'ng-jhipster';
import { StateStorageService } from '../providers/rest/state-storage.service';
import { HeaderOrderProvider } from '../pages/header-order/header-orders.service';
import { SuratJalanProvider } from '../pages/surat-jalan/surat-jalan.service';
import { HeaderOrderListPage } from '../pages/header-order/header-order-list';
import { LoadingService } from '../providers/loading-service/loading-service';
import { SuratJalanListPage } from '../pages/surat-jalan/surat-jalan-list';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    HeaderOrderListPage,
    SuratJalanListPage,

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-' }),
    NgbModule.forRoot(),
    HttpModule,
    IonicModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    HeaderOrderListPage,
    SuratJalanListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    RestProvider,
    LoadingService,
    NgbActiveModal,
    CookieService,
    LoginService,
    RestProvider,
    JhiEventManager,
    StateStorageService,
    HeaderOrderProvider,
    SuratJalanProvider
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
