import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ResponseWrapper } from '../../app/shared/model/response-wrapper.model';
import { SuratjalanModel } from './surat-jalan.models';
import { SuratJalanProvider } from './surat-jalan.service';

/**
 * Generated class for the HeaderOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-surat-jalan-list',
  templateUrl: 'surat-jalan-list.html',
})
export class SuratJalanListPage {
  public listSJGlobals: SuratjalanModel[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private suratJalanProvider: SuratJalanProvider) {
  }

  ionViewDidLoad() {
    if ((this.navParams.get('periode') !== null || this.navParams.get('periode') !== '') &&
      (this.navParams.get('bulan') !== null || this.navParams.get('bulan') !== '') &&
      (this.navParams.get('tahun') !== null || this.navParams.get('tahun') !== '')) {
      this.getDataSJGlobal();
    }
  }

  getDataSJGlobal() {
    this.suratJalanProvider.getSJGlobalList({
      periode: this.navParams.get('periode'),
      bulan: this.navParams.get('bulan'),
      tahun: this.navParams.get('tahun')
    }).subscribe(
      (res: ResponseWrapper) => { this.listSJGlobals = res.json })
  }

  getItems(ev) {
    var val = ev.target.value;
    if (val && val.trim() != '') {
      this.listSJGlobals = this.listSJGlobals.filter((item) => {
        return (item.no_po.indexOf(val) > -1);
      })
    } else {
      this.getDataSJGlobal()
    }

  }
}
