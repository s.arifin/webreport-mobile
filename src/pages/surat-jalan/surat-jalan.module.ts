import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SuratJalanPage } from './surat-jalan';

@NgModule({
  declarations: [
    SuratJalanPage,
  ],
  imports: [
    IonicPageModule.forChild(SuratJalanPage),
  ],
})
export class SuratJalanPageModule {}
