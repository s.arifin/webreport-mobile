import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs } from '@angular/http';
import { ResponseWrapper } from '../../app/shared/model/response-wrapper.model';
import { createRequestOption } from '../../app/shared/model/request-util';
import { Observable } from 'rxjs/Rx';
import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';
import { Resources } from '../../resouce';
import { map } from 'rxjs/operators';

@Injectable()
export class SuratJalanProvider {

    // private apiUrl = 'https://restcountries.eu/rest/v2/all';
    private baseURL = Resources.Constants.API.MY_URL;


    constructor(
        public http: Http,
        private localStorage: LocalStorageService,
        private sessionStorage: SessionStorageService) {
        console.log('Hello RestProvider Provider');
    }

    requestIntercept(options?: RequestOptionsArgs): RequestOptionsArgs {
        const token = this.localStorage.retrieve('authenticationToken') || this.sessionStorage.retrieve('authenticationToken');
        if (!!token) {
            options.headers.append('Authorization', 'Bearer ' + token);
        }
        return options;
    }

    responseIntercept(observable: Observable<Response>): Observable<Response> {
        return observable; // by pass
    }


    getSJGlobalTotal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.baseURL + '/api/surat-jalans', this.requestIntercept(options))
            .pipe(map((res: Response) => this.convertResponse(res)));
    }

    getSJGlobalList(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.baseURL + '/api/header-orders/po-global', this.requestIntercept(options))
            .pipe(map((res: Response) => this.convertResponse(res)));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }
}
