import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ResponseWrapper } from '../../app/shared/model/response-wrapper.model';
import { LoginService } from '../login/login.service';
import { LoginPage } from '../login/login';
import { HeaderOrderProvider } from '../header-order/header-orders.service';
import { SuratJalanProvider } from '../surat-jalan/surat-jalan.service';
import { HeaderOrderListPage } from '../header-order/header-order-list';
import { SuratJalanListPage } from '../surat-jalan/surat-jalan-list';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public bulan: Number;
  public tahun: number;
  public bulanSTR: string;
  public tahun2digit: number;
  public dt: Date = new Date();
  public periode: String = '';
  public jumlahPO: String = '';
  public jumlahSJ: String = '';

  constructor(
    public navCtrl: NavController,
    private headerOrderService: HeaderOrderProvider,
    private suratJalanService: SuratJalanProvider,
    private loginService: LoginService,
  ) {

  }

  ionViewDidLoad() {
    this.inisializePeriode()
      .then(
        (res: any) => { this.loadData() }
      );
  }

  getDataPoGlobal() {
    this.headerOrderService.getPoGlobalTotal({
      periode: this.bulanSTR + (this.tahun - 2000)
    }).subscribe(
      (res: ResponseWrapper) => { this.jumlahPO = res.headers.get('X-Total-Count') }
    )
  }

  getDataSJGlobal() {
    this.suratJalanService.getSJGlobalTotal({
      periode: this.bulanSTR + (this.tahun - 2000)
    }).subscribe(
      (res: ResponseWrapper) => { this.jumlahSJ = res.headers.get('X-Total-Count') }
    )
  }

  inisializePeriode(): Promise<any> {
    return new Promise<any>(
      (resolve, reject) => {
        this.bulan = this.dt.getUTCMonth() + 1;
        if (this.bulan.toString().length === 1) {
          this.bulanSTR = '0' + this.bulan.toString();
        }
        this.tahun = this.dt.getFullYear();
        console.log('data Bulan = ' + this.bulan);
        console.log('data Tahun = ' + this.tahun);

        this.getPeriodeString(this.bulanSTR, this.tahun.toString());
        resolve();
      }
    )
  }

  getPeriodeString(bulanP: string, tahunP: string): String {
    if (bulanP.includes('0') === true) {
      bulanP = bulanP.substring(1, 2);
    }
    const montArr = [];
    montArr[0] = 'Januari';
    montArr[1] = 'Februari';
    montArr[2] = 'Maret';
    montArr[3] = 'April';
    montArr[4] = 'Mei';
    montArr[5] = 'Juni';
    montArr[6] = 'Juli';
    montArr[7] = 'Agustus';
    montArr[8] = 'September';
    montArr[9] = 'Oktober';
    montArr[10] = 'November';
    montArr[11] = 'Januari';
    this.periode = montArr[parseInt(bulanP, 10) - 1] + '  ' + tahunP;
    return this.periode;
  }

  logout() {
    this.loginService.logout();
    this.navCtrl.push(LoginPage);
  }

  loadData() {
    this.getDataPoGlobal();
    this.getDataSJGlobal();
  }

  detailPO(){
    this.navCtrl.push(HeaderOrderListPage, {'periode':(this.bulanSTR + (this.tahun - 2000)), 'bulan':this.bulanSTR, 'tahun':(this.tahun)})
  }
  detailSJ(){
    this.navCtrl.push(SuratJalanListPage, {'periode':(this.bulanSTR + (this.tahun - 2000)), 'bulan':this.bulanSTR, 'tahun':(this.tahun)})
  }

}
