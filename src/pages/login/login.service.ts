import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Resources } from '../../resouce';
import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

// import { Principal } from '../auth/principal.service';
// import { AuthServerProvider } from '../auth/auth-jwt.service';

@Injectable()
export class LoginService {
    private http: Http;

    constructor(
        private $localStorage: LocalStorageService,
        private $sessionStorage: SessionStorageService,
        _http: Http
        // private principal: Principal,
        // private authServerProvider: AuthServerProvider
    ) { 
        this.http = _http;
    }

    login(credentials, callback?) {
        const cb = callback || function() {};

        return new Promise((resolve, reject) => {
            this.getlogin(credentials).subscribe((data) => {
                // this.principal.identity(true).then((account) => {
                //     // After the login the language will be changed to
                //     // the language selected by the user during his registration
                //     if (account !== null) {
                //         this.languageService.changeLanguage(account.langKey);
                //     }
                //     resolve(data);
                // });
                return cb();
            }, (err) => {
                this.logout();
                reject(err);
                return cb(err);
            });
        });
    }

    getlogin(credentials): Observable<any> {

        const data = {
            username: credentials.username,
            password: credentials.password,
            rememberMe: credentials.rememberMe
        };
        return this.http.post(Resources.Constants.API.MY_URL + '/api/authenticate', data).pipe(map(authenticateSuccess.bind(this)));

        function authenticateSuccess(resp) {
            console.log('response = ', resp);
            const bearerToken = resp.headers.get('Authorization');
            if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
                const jwt = bearerToken.slice(7, bearerToken.length);
                this.storeAuthenticationToken(jwt, credentials.rememberMe);
                return jwt;
            }
        }
    }
    storeAuthenticationToken(jwt, rememberMe) {
        if (rememberMe) {
            this.$localStorage.store('authenticationToken', jwt);
        } else {
            this.$sessionStorage.store('authenticationToken', jwt);
        }
    }

    logout() {
        this.setlogout().subscribe();
        // this.principal.authenticate(null);
        document.body.classList.add('not-login');
        document.body.classList.remove('hide-loading');
    }

    setlogout(): Observable<any> {
        return new Observable((observer) => {
            this.$localStorage.clear('authenticationToken');
            this.$sessionStorage.clear('authenticationToken');
            observer.complete();
        });
    }
} 