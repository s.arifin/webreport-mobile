import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LoginService } from './login.service';
import { HomePage } from '../home/home';
import { ResponseWrapper } from '../../app/shared/model/response-wrapper.model';
import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';
import { LoadingService } from '../../providers/loading-service/loading-service';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  authenticationError: boolean;
  password: string;
  rememberMe: boolean;
  username: string;
  credentials: any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loginService: LoginService,
    public alertController: AlertController,
    private $localStorage: LocalStorageService,
    private $sessionStorage: SessionStorageService,
    private loadingService: LoadingService,
  ) {
  }

  ionViewDidLoad() {
    this.loginValidation();
  }

  login() {
    this.loadingService.startLoading();
    this.loginService.getlogin({
      username: this.username,
      password: this.password,
      rememberMe: this.rememberMe
    }).subscribe(
      (res: ResponseWrapper) => {this.navCtrl.setRoot(TabsPage), this.loadingService.stopLoading()},
      (err: Response) => {this.loadingService.stopLoading()})
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      title: 'ERROR LOGIN',
      message: 'Username dan atau Password salah',
      buttons: ['OK']
    });

    await alert.present();
  }

  loginValidation() {
    const token = this.$localStorage.retrieve('authenticationToken') || this.$sessionStorage.retrieve('authenticationToken');
    if (!!token) {
      this.navCtrl.push(HomePage);
    }
  }

}
