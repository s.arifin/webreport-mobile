import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HeaderOrderProvider } from './header-orders.service';
import { HeaderOrdersModel } from './header-order.models';
import { ResponseWrapper } from '../../app/shared/model/response-wrapper.model';

/**
 * Generated class for the HeaderOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-header-order-list',
  templateUrl: 'header-order-list.html',
})
export class HeaderOrderListPage {
  public listPoGlobals: HeaderOrdersModel[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private headerOrderService: HeaderOrderProvider) {
  }

  ionViewDidLoad() {
    if ((this.navParams.get('periode') !== null || this.navParams.get('periode') !== '') &&
      (this.navParams.get('bulan') !== null || this.navParams.get('bulan') !== '') &&
      (this.navParams.get('tahun') !== null || this.navParams.get('tahun') !== '')) {
      this.getDataPoGlobal();
    }
  }

  getDataPoGlobal() {
    this.headerOrderService.getPOGlobalList({
      periode: this.navParams.get('periode'),
      bulan: this.navParams.get('bulan'),
      tahun: this.navParams.get('tahun')
    }).subscribe(
      (res: ResponseWrapper) => { this.listPoGlobals = res.json })
  }

  getItems(ev) {
    var val = ev.target.value;
    if (val && val.trim() != '') {
      this.listPoGlobals = this.listPoGlobals.filter((item) => {
        return (item.no_po.indexOf(val) > -1);
      })
    } else {
      this.getDataPoGlobal()
    }

  }
}
