import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HeaderOrderPage } from './header-order';
import { HeaderOrderListPage } from './header-order-list';

@NgModule({
  declarations: [
    HeaderOrderPage
  ],
  imports: [
    IonicPageModule.forChild(HeaderOrderPage),
  ],
  exports:[
    HeaderOrderListPage
  ]
})
export class HeaderOrderPageModule {}
