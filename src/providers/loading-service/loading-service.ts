import { LoadingController } from 'ionic-angular';
import { Injectable } from '@angular/core';

@Injectable()
export class LoadingService {
    constructor(
        public loadingCtrl: LoadingController,
    ) { }

    public loader = this.loadingCtrl.create({ content: "Please wait...", duration: 3000 });

    public startLoading(){
        this.loader.present()
    }

    public stopLoading(){
        this.loader.dismiss();
    }
}
