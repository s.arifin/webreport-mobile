import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs } from '@angular/http';
import { ResponseWrapper } from '../../app/shared/model/response-wrapper.model';
import { Observable } from 'rxjs/Rx';
import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';
@Injectable()
export class RestProvider {


  constructor(
    public http: Http,
    private localStorage: LocalStorageService,
    private sessionStorage: SessionStorageService) {
    console.log('Hello RestProvider Provider');
  }

  requestIntercept(options?: RequestOptionsArgs): RequestOptionsArgs {
    const token = this.localStorage.retrieve('authenticationToken') || this.sessionStorage.retrieve('authenticationToken');
    if (!!token) {
      options.headers.append('Authorization', 'Bearer ' + token);
    }
    return options;
  }

  responseIntercept(observable: Observable<Response>): Observable<Response> {
    return observable; // by pass
  }

  // private convertResponse(res: Response): ResponseWrapper {
  //   const jsonResponse = res.json();
  //   return new ResponseWrapper(res.headers, jsonResponse, res.status);
  // }
}
